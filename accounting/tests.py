#!/user/bin/env python2.7

import unittest
from datetime import date, datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from nose.tools import set_trace
from accounting import db
from models import Contact, Invoice, Payment, Policy
from utils import PolicyAccounting

"""
#######################################################
Test Suite for Accounting
#######################################################
"""

class TestBillingSchedules(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        db.session.add(cls.policy)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        pass

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        db.session.commit()

    def test_annual_billing_schedule(self):
        self.policy.billing_schedule = "Annual"
        #No invoices currently exist
        self.assertFalse(self.policy.invoices)
        #Invoices should be made when the class is initiated
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(len(self.policy.invoices), 1)
        self.assertEquals(self.policy.invoices[0].amount_due, self.policy.annual_premium)

    def test_monthly_billing_schedule(self):
        self.policy.billing_schedule = "Monthly"
        #No invoices currently exist
        self.assertFalse(self.policy.invoices)
        #Invoices should be made when the class is initiated
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(len(self.policy.invoices), 12)
        self.assertEquals(self.policy.invoices[0].amount_due, self.policy.annual_premium / 12)


class TestReturnAccountBalance(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_agent = Contact('Test Agent', 'Agent')
        cls.test_insured = Contact('Test Insured', 'Named Insured')
        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.commit()

        cls.policy = Policy('Test Policy', date(2015, 1, 1), 1200)
        cls.policy.named_insured = cls.test_insured.id
        cls.policy.agent = cls.test_agent.id
        db.session.add(cls.policy)
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.policy)
        db.session.commit()

    def setUp(self):
        self.payments = []

    def tearDown(self):
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        for payment in self.payments:
            db.session.delete(payment)
        db.session.commit()

    def test_annual_on_eff_date(self):
        self.policy.billing_schedule = "Annual"
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(pa.return_account_balance(date_cursor=self.policy.effective_date), 1200)

    def test_quarterly_on_eff_date(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        self.assertEquals(pa.return_account_balance(date_cursor=self.policy.effective_date), 300)

    def test_quarterly_on_last_installment_bill_date(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        invoices = Invoice.query.filter_by(policy_id=self.policy.id)\
                                .order_by(Invoice.bill_date).all()
        self.assertEquals(pa.return_account_balance(date_cursor=invoices[3].bill_date), 1200)

    def test_quarterly_on_second_installment_bill_date_with_full_payment(self):
        self.policy.billing_schedule = "Quarterly"
        pa = PolicyAccounting(self.policy.id)
        invoices = Invoice.query.filter_by(policy_id=self.policy.id)\
                                .order_by(Invoice.bill_date).all()
        self.payments.append(pa.make_payment(contact_id=self.policy.agent,
                                             date_cursor=invoices[1].bill_date, amount=600))
        self.assertEquals(pa.return_account_balance(date_cursor=invoices[1].bill_date), 0)

class TestEvaluateCancellationPendingDueToNonPay(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        agent = Contact('Test Agent', 'Agent')
        insured = Contact('Test Insured', 'Named Insured')
        policy = Policy('Test Policy', datetime(2019, 2, 10), 1200)
        policy.agent = agent.id
        policy.named_insured = insured.id
        policy.billing_schedule = 'Monthly'
        cls.test_policy = policy
        cls.test_agent = agent
        cls.test_insured = insured

        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.add(cls.test_policy)
        db.session.commit()


    @classmethod
    def tearDownClass(cls):
        for invoice in cls.test_policy.invoices:
            db.session.delete(invoice)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_policy)
        db.session.commit()

    def tearDown(self):
        payments = Payment.query.filter_by(policy_id=self.test_policy.id).all()
        if len(payments) > 0:
            for payment in payments:
                db.session.delete(payment)
                db.session.commit()

    
    def test_returns_yes(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=14)
        evaluation = pa.evaluate_cancellation_pending_due_to_non_pay(date_cursor)
        self.assertTrue(evaluation)
        self.assertEquals(self.test_policy.status, "Cancellation Pending")

    def test_returns_false(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=13)
        for invoice in self.test_policy.invoices:
            pa.make_payment(self.test_agent.id, invoice.due_date, invoice.amount_due)
        self.assertFalse(pa.evaluate_cancellation_pending_due_to_non_pay(date_cursor))

    def test_only_agents_can_pay_cancellation_pending_due_to_non_pay(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=13)
        for invoice in self.test_policy.invoices:
            pa.make_payment(self.test_insured.id, invoice.due_date, invoice.amount_due)

        self.assertTrue(pa.return_account_balance(date_cursor) > 0)


class TestChangeBillingSchedule(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        agent = Contact('Test Agent', 'Agent')
        insured = Contact('Test Insured', 'Named Insured')
        policy = Policy('Test Policy', datetime.now().date(), 1200)
        policy.agent = agent.id
        policy.named_insured = insured.id
        policy.billing_schedule = 'Monthly'
        cls.test_policy = policy
        cls.test_agent = agent
        cls.test_insured = insured

        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.add(cls.test_policy)
        db.session.commit()


    @classmethod
    def tearDownClass(cls):
        for invoice in cls.test_policy.invoices:
            db.session.delete(invoice)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_policy)
        db.session.commit()

    def tearDown(self):
        payments = Payment.query.filter_by(policy_id=self.test_policy.id).all()
        if len(payments) > 0:
            for payment in payments:
                db.session.delete(payment)
                db.session.commit()

    def test_changed(self):
        pa = PolicyAccounting(self.test_policy.id)
        pa.change_billing_schedule('Quarterly')
        self.assertEquals(len(self.test_policy.invoices), 4)
        pa.change_billing_schedule('Monthly')
        self.assertEquals(len(self.test_policy.invoices), 12)

    def test_invalid_billing_schedule(self):
        pa = PolicyAccounting(self.test_policy.id)
        pa.change_billing_schedule('asdaasoidja')
        self.assertEquals(len(self.test_policy.invoices), 12)

    def test_amount_left_is_unchanged(self):
        pa = PolicyAccounting(self.test_policy.id)
        # account balance 100 because 1200 / 12 == 100
        self.assertEquals(pa.return_account_balance(), 100)
        pa.make_payment(self.test_agent.id, datetime.now().date(), amount=100)
        self.assertEquals(pa.return_account_balance(), 0)

        pa.change_billing_schedule('Quarterly')
        self.assertEquals(len(self.test_policy.invoices), 4)
        # account balance 200 because 1200 / 4 = 300 - 100 = 200
        self.assertEquals(pa.return_account_balance(), 200)

class TestEvaluateCancel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        agent = Contact('Test Agent', 'Agent')
        insured = Contact('Test Insured', 'Named Insured')
        policy = Policy('Test Policy', datetime(2019, 2, 10), 1200)
        policy.agent = agent.id
        policy.named_insured = insured.id
        policy.billing_schedule = 'Monthly'
        cls.test_policy = policy
        cls.test_agent = agent
        cls.test_insured = insured

        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.add(cls.test_policy)
        db.session.commit()


    @classmethod
    def tearDownClass(cls):
        for invoice in cls.test_policy.invoices:
            db.session.delete(invoice)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_policy)
        db.session.commit()

    def tearDown(self):
        payments = Payment.query.filter_by(policy_id=self.test_policy.id).all()
        if len(payments) > 0:
            for payment in payments:
                db.session.delete(payment)
                db.session.commit()

    
    def test_returns_true(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=14)
        evaluation = pa.evaluate_cancel(date_cursor)
        self.assertTrue(evaluation)
        self.assertEquals(self.test_policy.status, "Canceled")


    def test_returns_false(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=13)
        for invoice in self.test_policy.invoices:
            pa.make_payment(self.test_agent.id, invoice.due_date, invoice.amount_due)
        self.assertFalse(pa.evaluate_cancel(date_cursor))

class TestCancelPolicy(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        agent = Contact('Test Agent', 'Agent')
        insured = Contact('Test Insured', 'Named Insured')
        policy = Policy('Test Policy', datetime(2019, 2, 10), 1200)
        policy.agent = agent.id
        policy.named_insured = insured.id
        policy.billing_schedule = 'Monthly'
        cls.test_policy = policy
        cls.test_agent = agent
        cls.test_insured = insured

        db.session.add(cls.test_agent)
        db.session.add(cls.test_insured)
        db.session.add(cls.test_policy)
        db.session.commit()


    @classmethod
    def tearDownClass(cls):
        for invoice in cls.test_policy.invoices:
            db.session.delete(invoice)
        db.session.delete(cls.test_agent)
        db.session.delete(cls.test_insured)
        db.session.delete(cls.test_policy)
        db.session.commit()

    def tearDown(self):
        payments = Payment.query.filter_by(policy_id=self.test_policy.id).all()
        if len(payments) > 0:
            for payment in payments:
                db.session.delete(payment)
                db.session.commit()

    
    def test_cancel_if_evaluation_returns_true(self):
        pa = PolicyAccounting(self.test_policy.id)
        date_cursor = datetime(2019, 2, 10) + relativedelta(months=1, days=14)
        
        pa.cancel(reason="Underwriting", date=date_cursor)
        self.assertEquals(self.test_policy.status, "Canceled")
        self.assertEquals(self.test_policy.cancel_date, date_cursor.date())
        self.assertEquals(self.test_policy.cancel_reason, "Underwriting")

