#!/user/bin/env python2.7

from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from accounting import db
from models import Contact, Invoice, Payment, Policy

"""
#######################################################
This is the base code for the engineer project.
#######################################################
"""

class PolicyAccounting(object):
    billing_schedules = {
        'Annual': 1,
        'Semi-Annual': 3,
        'Quarterly': 4,
        'Monthly': 12,
        'Two-Pay': 2
    }

    """
     Each policy has its own instance of accounting.
    """
    def __init__(self, policy_id):
        self.policy = Policy.query.filter_by(id=policy_id).one()

        if not self.policy.invoices:
            self.make_invoices()

    def change_billing_schedule(self, billing_schedule):
        """
            Receives a string that has to contain one of the billing options
            otherwise it wont do any change and print a string instead
        """
        new_billing_schedule = self.billing_schedules.get(billing_schedule)
        if not new_billing_schedule:
            print "Invalid Billing Schedule"
            return
        self.policy.billing_schedule = billing_schedule
        if self.policy.invoices:
            for invoice in self.policy.invoices:
                db.session.delete(invoice)
        self.make_invoices()


    def return_account_balance(self, date_cursor=None):
        """
            it receives a date thats gonna be used to get all the invoices and payments
            up until that date, and it will do a sum between what was already paid and 
            whats left to paid, thats the account balance
        """
        if not date_cursor:
            date_cursor = datetime.now().date()

        invoices = Invoice.query.filter_by(policy_id=self.policy.id)\
                                .filter(Invoice.bill_date <= date_cursor)\
                                .order_by(Invoice.bill_date)\
                                .all()
        due_now = 0
        for invoice in invoices:
            due_now += invoice.amount_due

        payments = Payment.query.filter_by(policy_id=self.policy.id)\
                                .filter(Payment.transaction_date <= date_cursor)\
                                .all()
        for payment in payments:
            due_now -= payment.amount_paid

        return due_now

    def make_payment(self, contact_id=None, date_cursor=None, amount=0):
        """
            receives who paid is paying, for which date and the amount,
            it creates a payment and commits it to the db
        """
        if not date_cursor:
            date_cursor = datetime.now().date()

        if not contact_id:
            try:
                contact_id = self.policy.named_insured
            except:
                pass
        
        contact = Contact.query.get(contact_id)
        if self.evaluate_cancellation_pending_due_to_non_pay(date_cursor) and contact.role == 'Agent':

            payment = Payment(self.policy.id,
                              contact_id,
                              amount,
                              date_cursor)
            db.session.add(payment)
            db.session.commit()

            return payment
        else:
            print "Only an agent can make a payment"
            return None

    def evaluate_cancellation_pending_due_to_non_pay(self, date_cursor=None):
        """
         If this function returns true, an invoice
         on a policy has passed the due date without
         being paid in full. However, it has not necessarily
         made it to the cancel_date yet.
        """
        if not date_cursor:
            date_cursor = datetime.now().date()

        due_invoices = Invoice.query.filter_by(policy_id=self.policy.id)\
                                    .filter(Invoice.due_date <= date_cursor)\
                                    .filter(Invoice.cancel_date > date_cursor)\
                                    .order_by(Invoice.due_date)\
                                    .all()
        for invoice in due_invoices:
            if not self.return_account_balance(invoice.due_date) > 0:
               return False 
        else:
            self.policy.status = 'Cancellation Pending'
            db.session.commit()
            return True

    def cancel(self, reason=None, date=None):
        if not date:
            date = datetime.now().date()

        if self.evaluate_cancel(date):
            self.policy.cancel_date = date
            self.policy.cancel_reason = reason
            db.session.commit()



    def evaluate_cancel(self, date_cursor=None):
        """
            it receives a date which pulls all the invoices where the cancellation was up until 
            that date to check all the invoices and see if a policy should be cancelled or not, returns true if a policy should be cancelled, returns no if not
        """
        if not date_cursor:
            date_cursor = datetime.now().date()

        invoices = Invoice.query.filter_by(policy_id=self.policy.id)\
                                .filter(Invoice.cancel_date <= date_cursor)\
                                .order_by(Invoice.bill_date)\
                                .all()

        for invoice in invoices:
            if not self.return_account_balance(invoice.cancel_date):
                continue
            else:
                self.policy.status = 'Canceled'
                db.session.commit()
                print "THIS POLICY SHOULD HAVE CANCELED"
                return True
        else:
            print "THIS POLICY SHOULD NOT CANCEL"
            return False


    def make_invoices(self):
        """
            it removes all the invoices from a policy and creates new ones
            according to the policy billing schedule
        """
        for invoice in self.policy.invoices:
            db.session.delete(invoice)
        db.session.commit()


        invoices = []
        first_invoice = Invoice(self.policy.id,
                                self.policy.effective_date, #bill_date
                                self.policy.effective_date + relativedelta(months=1), #due
                                self.policy.effective_date + relativedelta(months=1, days=14), #cancel
                                self.policy.annual_premium)
        invoices.append(first_invoice)

        if self.policy.billing_schedule == "Annual":
            first_invoice.amount_due = first_invoice.amount_due / self.billing_schedules.get(self.policy.billing_schedule)
            for i in range(1, self.billing_schedules.get(self.policy.billing_schedule)):
                months_after_eff_date = i*1
                bill_date = self.policy.effective_date + relativedelta(months=months_after_eff_date)
                invoice = Invoice(self.policy.id,
                                  bill_date,
                                  bill_date + relativedelta(months=1),
                                  bill_date + relativedelta(months=1, days=14),
                                  self.policy.annual_premium / self.billing_schedules.get(self.policy.billing_schedule))
                invoices.append(invoice)

        elif self.policy.billing_schedule == "Two-Pay":
            first_invoice.amount_due = first_invoice.amount_due / self.billing_schedules.get(self.policy.billing_schedule)
            for i in range(1, self.billing_schedules.get(self.policy.billing_schedule)):
                months_after_eff_date = i*6
                bill_date = self.policy.effective_date + relativedelta(months=months_after_eff_date)
                invoice = Invoice(self.policy.id,
                                  bill_date,
                                  bill_date + relativedelta(months=1),
                                  bill_date + relativedelta(months=1, days=14),
                                  self.policy.annual_premium / self.billing_schedules.get(self.policy.billing_schedule))
                invoices.append(invoice)
        elif self.policy.billing_schedule == "Quarterly":
            first_invoice.amount_due = first_invoice.amount_due / self.billing_schedules.get(self.policy.billing_schedule)
            for i in range(1, self.billing_schedules.get(self.policy.billing_schedule)):
                months_after_eff_date = i*3
                bill_date = self.policy.effective_date + relativedelta(months=months_after_eff_date)
                invoice = Invoice(self.policy.id,
                                  bill_date,
                                  bill_date + relativedelta(months=1),
                                  bill_date + relativedelta(months=1, days=14),
                                  self.policy.annual_premium / self.billing_schedules.get(self.policy.billing_schedule))
                invoices.append(invoice)
        elif self.policy.billing_schedule == "Monthly":
            first_invoice.amount_due = first_invoice.amount_due / self.billing_schedules.get(self.policy.billing_schedule)
            for i in range(1, self.billing_schedules.get(self.policy.billing_schedule)):
                months_after_eff_date = i*12
                bill_date = self.policy.effective_date + relativedelta(months=months_after_eff_date)
                invoice = Invoice(self.policy.id,
                                  bill_date,
                                  bill_date + relativedelta(months=1),
                                  bill_date + relativedelta(months=1, days=14),
                                  self.policy.annual_premium / self.billing_schedules.get(self.policy.billing_schedule))
                invoices.append(invoice)
        else:
            print "You have chosen a bad billing schedule."

        for invoice in invoices:
            db.session.add(invoice)
        db.session.commit()

################################
# The functions below are for the db and 
# shouldn't need to be edited.
################################
def build_or_refresh_db():
    db.drop_all()
    db.create_all()
    insert_data()
    print "DB Ready!"

def insert_data():
    #Contacts
    contacts = []
    john_doe_agent = Contact('John Doe', 'Agent')
    contacts.append(john_doe_agent)
    john_doe_insured = Contact('John Doe', 'Named Insured')
    contacts.append(john_doe_insured)
    bob_smith = Contact('Bob Smith', 'Agent')
    contacts.append(bob_smith)
    anna_white = Contact('Anna White', 'Named Insured')
    contacts.append(anna_white)
    joe_lee = Contact('Joe Lee', 'Agent')
    contacts.append(joe_lee)
    ryan_bucket = Contact('Ryan Bucket', 'Named Insured')
    contacts.append(ryan_bucket)

    for contact in contacts:
        db.session.add(contact)
    db.session.commit()

    policies = []
    p1 = Policy('Policy One', date(2015, 1, 1), 365)
    p1.billing_schedule = 'Annual'
    p1.agent = bob_smith.id
    policies.append(p1)

    p2 = Policy('Policy Two', date(2015, 2, 1), 1600)
    p2.billing_schedule = 'Quarterly'
    p2.named_insured = anna_white.id
    p2.agent = joe_lee.id
    policies.append(p2)

    p3 = Policy('Policy Three', date(2015, 1, 1), 1200)
    p3.billing_schedule = 'Monthly'
    p3.named_insured = ryan_bucket.id
    p3.agent = john_doe_agent.id
    policies.append(p3)

    p4 = Policy('Policy Four', datetime(2015, 2, 1), 500)
    p4.billing_schedule = 'Two-Pay'
    p4.agent = john_doe_agent.id 
    p4.named_insured = ryan_bucket.id 
    policies.append(p4)

    for policy in policies:
        db.session.add(policy)
    db.session.commit()

    for policy in policies:
        PolicyAccounting(policy.id)

    payment_for_p2 = Payment(p2.id, anna_white.id, 400, date(2015, 2, 1))
    db.session.add(payment_for_p2)
    db.session.commit()

