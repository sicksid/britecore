#!/usr/bin/env python
from accounting import *
from accounting.models import *
from accounting.utils import *
from flask import *

try:
    from IPython import embed
    embed()
except ImportError:
    import os
    # for macos, using gnureadline because we dont have support for readline.so 
    # and i dont want to use macports just now
    try:
        import gnureadline as readline 
    except ImportError:
        import readline
    from pprint import pprint
    os.environ['PYTHONINSPECT'] = 'True'
